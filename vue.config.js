/*
 * @Description: 全局配置文件
 * @Author: John Holl
 * @Github: https://github.com/hzylyh
 * @Date: 2021-03-31 20:57:26
 * @LastEditors: John Holl
 */

const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir);
}
module.exports = {
    publicPath: './',
    devServer: {
        proxy: {
            // 接口拦截转发
            '/cqApiH5/': {
                target: 'http://192.168.1.161:xxx',
                ws: false,
                pathRewrite: {
                    '^/cqApiH5/': '/'
                }
            }
        }
    },
    configureWebpack: {
        // provide the app's title in webpack's name field, so that
        // it can be accessed in index.html to inject the correct title.
        name: 'android-h5-demo',
        resolve: {
            alias: {
                '@': resolve('src'),
                '@assets': resolve('src/assets'),
                '@view': resolve('src/views')
            }
        }
    },
    css: {
        loaderOptions: {}
    }
};
