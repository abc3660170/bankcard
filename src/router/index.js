import Vue from 'vue';
import VueRouter from 'vue-router';
//import UserInfo from '@view/user/children/userInfo.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "login" */ '../views/login')
    },
    {
        path: '/service',
        name: 'service',
        component: () => import(/* webpackChunkName: "service" */ '../views/service')
    },
    {
        path: '/user',
        component: () => import(/* webpackChunkName: "user" */ '../views/user/index.vue'),
        children: [
            {
                path: '/',
                component: () =>
                    import(/* webpackChunkName: "user" */ '../views/user/children/UserInfo.vue'),
                name: 'user'
            },
            {
                path: 'info',
                component: () =>
                    import(/* webpackChunkName: "user" */ '../views/user/children/EditInfo.vue'),
                name: 'userInfo'
            },
            {
                path: 'version',
                component: () =>
                    import(/* webpackChunkName: "user" */ '../views/user/children/VersionInfo.vue'),
                name: 'version'
            },
            {
                path: 'maintenance',
                component: () =>
                    import(
                        /* webpackChunkName: "user" */ '../views/user/children/MaintenanceInfo.vue'
                    ),
                name: 'maintenance'
            },
            {
                path: 'setting',
                component: () =>
                    import(/* webpackChunkName: "user" */ '../views/user/children/UserSet.vue'),
                name: 'setting'
            },
            {
                path: 'pwd',
                component: () =>
                    import(/* webpackChunkName: "user" */ '../views/user/children/EditPwd.vue'),
                name: 'pwd'
            },
            {
                path: 'score',
                component: () =>
                    import(/* webpackChunkName: "user" */ '../views/user/children/ScoreInfo.vue'),
                name: 'score'
            }
        ]
    },
    {
        path: '/record',
        component: () => import(/* webpackChunkName: "record" */ '../views/record/index.vue'),
        children: [
            {
                path: '/',
                component: () =>
                    import(
                        /* webpackChunkName: "user" */ '../views/record/children/RecordInfo.vue'
                    ),
                name: 'record'
            },
            {
                path: 'collect',
                component: () =>
                    import(
                        /* webpackChunkName: "user" */ '../views/record/children/CollectInfo.vue'
                    ),
                name: 'collect'
            },
            {
                path: 'card',
                component: () =>
                    import(
                        /* webpackChunkName: "user" */ '../views/record/children/CollectCard.vue'
                    ),
                name: 'card'
            },{
                path: 'person',
                component: () =>
                    import(
                        /* webpackChunkName: "user" */ '../views/record/children/CollectPerson.vue'
                    ),
                name: 'person'
            },
            {
                path: 'cardinfo',
                component: () =>
                    import(
                        /* webpackChunkName: "user" */ '../views/record/children/EditCard.vue'
                    ),
                name: 'cardinfo'
            },
            {
                path: 'personinfo',
                component: () =>
                    import(
                        /* webpackChunkName: "user" */ '../views/record/children/EditPerson.vue'
                    ),
                name: 'personinfo'
            }
        ]
    }
];

const router = new VueRouter({
    routes
});

export default router;
