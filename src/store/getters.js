/*
 * @Description: getter汇总文件
 * @Author: John Holl
 * @Github: https://github.com/hzylyh
 * @Date: 2021-04-27 08:59:09
 * @LastEditors: John Holl
 */

const getters = {
    sidebar: state => state.app.sidebar
};

export default getters;
