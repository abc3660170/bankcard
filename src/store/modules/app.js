/*
 * @Description: 基础应用store
 * @Author: John Holl
 * @Github: https://github.com/hzylyh
 * @Date: 2021-03-31 21:48:57
 * @LastEditors: John Holl
 */

const state = {
    sidebar: {
        opened: localStorage.getItem('sidebarStatus')
            ? !!+localStorage.getItem('sidebarStatus')
            : true,
        withoutAnimation: false
    }
};

const mutations = {
    TOGGLE_SIDEBAR: state => {
        state.sidebar.opened = !state.sidebar.opened;
        state.sidebar.withoutAnimation = false;
        if (state.sidebar.opened) {
            localStorage.setItem('sidebarStatus', 1);
        } else {
            localStorage.setItem('sidebarStatus', 0);
        }
    },
    CLOSE_SIDEBAR: (state, withoutAnimation) => {
        localStorage.setItem('sidebarStatus', 0);
        state.sidebar.opened = false;
        state.sidebar.withoutAnimation = withoutAnimation;
    }
};

const actions = {
    toggleSideBar({ commit }) {
        commit('TOGGLE_SIDEBAR');
    },
    closeSideBar({ commit }, { withoutAnimation }) {
        commit('CLOSE_SIDEBAR', withoutAnimation);
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
};
