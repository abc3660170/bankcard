/*
 * @Description: jsbridge调用
 * @Author: John Holl
 * @Github: https://github.com/hzylyh
 * @Date: 2021-04-27 09:06:35
 * @LastEditors: John Holl
 */

// import store from '@/store'

const hybrid = {
    install(Vue) {
        // 把当前的对象挂载到vue的全局
        Vue.prototype.$hybrid = this;
    },
    // 供原生端调用，改变工单等数量
    changeOrderNum(num) {
        console.log(num);
        // store.dispatch('app/setOrderNum', num)
    }
};

// 把vue中绑定的对象挂载到window上
window.Hybrid = hybrid;

export default hybrid;
