/*
 * @Description: 请求封装
 * @Author: John Holl
 * @Github: https://github.com/hzylyh
 * @Date: 2021-03-31 20:55:29
 * @LastEditors: John Holl
 */

import axios from 'axios';
// import store from '@/store'

// create an axios instance
const service = axios.create({
    // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
    baseURL: '/cqApiH5', // url = base url + request url
    // withCredentials: true, // send cookies when cross-domain requests
    timeout: 5000 // request timeout
});

// request interceptor
service.interceptors.request.use(
    config => {
        // do something before request is sent
        config.headers.userToken = localStorage.getItem('userToken');
        return config;
    },
    error => {
        // do something with request error
        console.log(error); // for debug
        return Promise.reject(error);
    }
);

// response interceptor
service.interceptors.response.use(
    /**
     * If you want to get http information such as headers or status
     * Please return  response => response
     */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
    response => {
        const res = response.data;
        // if the custom code is not 20000, it is judged as an error.
        if (res.resultCode !== '2000') {
            // 错误信息提示

            return Promise.reject(new Error(res.message || 'Error'));
        }
        return res;
    },
    error => {
        console.log('err' + error); // for debug

        // 错误信息提示

        return Promise.reject(error);
    }
);

export default service;
