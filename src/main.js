import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Vant from 'vant';
import 'vant/lib/index.css';
import './styles/style.scss';

import './utils/flexible.js';
// 自定义插件
import hybrid from './utils/androidPlu';

Vue.config.productionTip = false;

// 引入
Vue.use(Vant);

// jsbridge
Vue.use(hybrid);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
