/*
 * @Description: 样例请求
 * @Author: John Holl
 * @Github: https://github.com/hzylyh
 * @Date: 2021-04-27 08:57:59
 * @LastEditors: John Holl
 */

import request from '@/utils/request'

const prefix = '/demo'
// 样例请求post
export function demoPost (param) {
  return request({
    url: prefix + '/demo/post',
    method: 'post',
    data: param
  })
}

// 样例请求get
export function demoGet (param) {
  return request({
    url: prefix + '/demo/get',
    method: 'get',
    params: param
  })
}
